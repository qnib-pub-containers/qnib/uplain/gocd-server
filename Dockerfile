ARG FROM_IMG_REGISTRY=registry.gitlab.com
ARG FROM_IMG_REPO=qnib-pub-containers/qnib/uplain
ARG FROM_IMG_NAME=openjdk-jre-headless
ARG FROM_IMG_TAG="2022-08-26.4"

FROM ${FROM_IMG_REGISTRY}/${FROM_IMG_REPO}/${FROM_IMG_NAME}:${FROM_IMG_TAG}

VOLUME ["/opt/go-server/artifacts/serverBackups/"]
ENV GOCD_AGENT_AUTOENABLE_KEY=qnibFTW \
    GOCD_SERVER_CLEAN_WORKSPACE=false
ARG GOCD_URL=https://download.gocd.io/binaries
ARG GOCD_VER=22.2.0
ARG GOCD_SUBVER=14697
ARG CT_VER=0.29.2
ENV GLIBC_REPO=https://github.com/sgerrand/alpine-pkg-glibc
ENV GLIBC_VERSION=2.30-r0

LABEL gocd.version=${GOCD_VER}-${GOCD_SUBVER}

RUN apt update \
 && apt install -y curl libarchive-tools python3-pip moreutils iproute2 git \
 && pip install wildq
RUN export SRC_URL=$(/usr/local/bin/go-github rLatestUrl --ghorg qnib --ghrepo service-scripts --regex ".*\.tar" |head -n1) \
 && echo "# service-scripts: ${SRC_URL}" \
 && curl -Ls ${SRC_URL} |tar xf - -C /opt/
RUN mkdir -p /opt/go-server \
 && echo "go-server: https://download.go.cd/binaries/${GOCD_VER}-${GOCD_SUBVER}/generic/go-server-${GOCD_VER}-${GOCD_SUBVER}.zip" \
 && curl -Ls --url ${GOCD_URL}/${GOCD_VER}-${GOCD_SUBVER}/generic/go-server-${GOCD_VER}-${GOCD_SUBVER}.zip |bsdtar xfz - -C /opt/go-server --strip-components=1
WORKDIR /opt/go-server/plugins/external/
# The layers are independently pushed, if they are combined one change will alter the content of the combined layer
RUN export GORG=gocd-contrib \
 && export GREPO=script-executor-task \
 && export SRC_URL=$(/usr/local/bin/go-github rLatestUrl --ghorg ${GORG} --ghrepo ${GREPO} --regex '.*\.jar' |head -n1) \
 && wget -q ${SRC_URL}
RUN export GORG=manojlds \
 && export GREPO=gocd-docker \
 && export SRC_URL=$(/usr/local/bin/go-github rLatestUrl --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*\.jar" |head -n1) \
 && wget -q ${SRC_URL}
RUN export GORG=ashwanthkumar \
 && export GREPO=gocd-build-github-pull-requests \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*github-pr-poller.*\.jar" |head -n1) \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*git-fb-poller.*\.jar" |head -n1)
RUN export GORG=ind9 \
 && export GREPO=gocd-s3-artifacts \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*s3material-assembly.*\.jar" |head -n1) \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*s3fetch-assembly.*\.jar" |head -n1) \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*s3publish-assembly.*\.jar" |head -n1)
RUN export GORG=jmnarloch \
 && export GREPO=gocd-health-check-plugin \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*\.jar" |head -n1)
RUN export GORG=gocd-contrib \
 && export GREPO=docker-swarm-elastic-agents \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*\.jar" |head -n1)
RUN export GORG=tomzo \
 && export GREPO=gocd-yaml-config-plugin \
 && wget -q $(/usr/local/bin/go-github rLatestUrl --ghorg gocd-contrib --ghorg ${GORG} --ghrepo ${GREPO} --regex ".*\.jar" |head -n1)
 
WORKDIR /root/
ADD opt/entry/10-gocd-restore.sh \
    opt/entry/11-gocd-config-init.sh \
    /opt/entry/
ENV GOCD_BACKUP_RESTORE_ENABLED=false
RUN chmod +x /opt/go-server/bin/go-server
CMD ["/opt/go-server/bin/go-server", "console"]
